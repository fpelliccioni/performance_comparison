# procesamos la fuente
indice = {}
fuente = open("src_shakesp.txt").read()
fuente = fuente.replace("\n", " ")
fuente = fuente.replace("\r", " ")
largo = len(fuente)
ini = 0
pos = 1

while pos < largo:
    f = fuente[pos]
    if f == " ":
        palabra = fuente[ini:pos]
        indice[palabra] = ini
        ini = pos + 1
    pos += 1

# revisamos que tenemos que encontrar
posmax = 0
palmax = None
for lin in open("src_tokens.txt"):
    pal = lin.strip()
    pos = indice[pal]
    if pos > posmax:
        posmax = pos
        palmax = pal
print palmax, posmax