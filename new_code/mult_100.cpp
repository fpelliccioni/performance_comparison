#include <cstdio>
#include <cstddef>

#include <algorithm>
#include <chrono>
#include <numeric>
#include <vector>

// Algorithm
inline
void mult(size_t size)
{
	const int a = 5;
	const int b = 10;

	for ( int i = 0; i < size; ++i )
	{
    	const int c = a * b;
    }
}



// Benchmarking infrastructure
using namespace std;
using namespace std::chrono;

uint64_t time_mult(size_t size) 
{
	auto start = std::chrono::steady_clock::now();

	mult(size);

	auto end = std::chrono::steady_clock::now();
	return std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
}

int main() 
{
  size_t min_size(8);
  size_t max_size(16 * 1024 * 1024);

  printf("           %12s %6s %6s\n", "size", "time", "log2");

  int lg = 3;
  for (size_t size(min_size); size <= max_size; size *= 2) 
  {
    uint64_t time = time_mult(size);
    printf("     [C++] %12lu %6llu %6.1f\n", size, time / max_size, (double)time / (max_size * lg));
    ++lg;
  }

  return 0;
}
