import time

# Algorithm
def mult(size):
	a = 5
	b = 10
	for i in xrange(size):
	    c = a * b

# Benchmarking infrastructure
def main():
  min_size = 8
  # max_size = 1024 * 1024
  max_size = 16 * 1024 * 1024

  print "           %12s %6s %6s" % ("size", "time", "log2")

  size = min_size
  lg = 3
  while size <= max_size:
    time = time_mult(buffer, size)
    print "  [Python] %12d %6d %6.1f" % (size, time // max_size, time / (max_size * lg))
    size *= 2
    lg += 1

def time_mult(buffer, size):
	start_time = time.time()
	mult(size)
	return (time.time() - start_time) * 1e9

if __name__ == "__main__":
  main()

