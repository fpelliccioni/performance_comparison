************************
 Performance Comparison
************************


We will create several algorithms to measure the performance of different programming languages
The initiative arise from the following talk (Spanish talk): 
	https://www.youtube.com/watch?v=cPVlYWxcu18
We encourage programmers to fix or upload versions of the programming language you feel comfortable.


*********
The tests
*********

1) mult_100: 
   This test comes from the talk mentioned above.
   
   The idea of the original creators of the test is: Multiply two numbers (always the same two numbers) and repeat exactly the same operation N-times.
   
   My idea is writing it and measuring it correctly to show that this test is useless.


*********
Platforms
*********

1) Processor: Intel(R) Core(TM) i7-3720QM CPU 2.6 GHz - Ivy Bridge microarchitecture.
   
   Memory: 8GB 1600 MHz DDR3
   
   Operating system: Mac OS X 10.9.2

   C++ compiler: clang version 3.5 (trunk 200620)
      clang++ -O3 -std=c++11 file_name.cpp

   Python interpreter: python 2.7.5
      python file_name.py

2) Processor: Intel(R) Core(TM) i7-4700MQ CPU 2.4 GHz - Haswell microarchitecture.
   
   Memory: 8GB 2400 MHz DDR3
   
   Operating system: Windows 8.1

   C++ compiler: GCC (MinGW)  version 4.8.0
      g++ -O3 -std=c++11 file_name.cpp

   Python interpreter: python 2.7.5
      python file_name.py




