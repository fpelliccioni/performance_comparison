#include <stdio.h>
#include <search.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>

typedef struct _poslist {
    int position;
    struct _poslist *next;
} poslist;


int main(void) {
    int in, red, count;
    FILE *out;
    struct stat st;
    char *text, *sep=" \n\r", *word; 
    ENTRY item, *found_item;
    poslist *node;
    
    in = open("src_shakesp.txt", O_RDONLY);
    fstat(in, &st);
    text = malloc(st.st_size+1);
    if (text==NULL) {
        printf("cagamos == %i\n", st.st_size);
        return(-1);
    }
    red = 0;
    while (red < st.st_size ) {
        red += read(in, text+red, 1024*8);
    }
    
    text[ st.st_size ] = 0;
    
    if ( hcreate(50000) == 0 ) {
        printf("sin espacio para hcreate\n");
        return (-1);
    }
    
    printf("creando indice\n");
    
    for (
            word = strtok(text, sep);
            word;
            word = strtok(NULL, sep)
         )
     {
        if (*word==0) continue;
        
        item.key = word;
        if ((found_item = hsearch(item, FIND)) != NULL) {
            // found
            node = malloc(sizeof(node));
            node->next = found_item->data;
            node->position = word-text;
            found_item->data = node;
        } else {
            // not found
            item.key = strdup(word);
            node = malloc(sizeof(node));
            node->next = NULL;
            node->position = word-text;
            item.data = node;
            (void) hsearch(item, ENTER);
        }
     }
     
     printf("buscando palabras\n");
    close(in);
    free(text);
    
    in = open("src_tokens.txt", O_RDONLY);
    fstat(in, &st);
    text = malloc(st.st_size+1);
    if (text==NULL) {
        printf("cagamos == %i\n", st.st_size);
        return(-1);
    }
    red = 0;
    while (red < st.st_size ) {
        red += read(in, text+red, 1024*8);
    }
    
    text[ st.st_size ] = 0;
    out = fopen("out_2.txt", "w");
    
    count = 0;
    for (
            word = strtok(text, sep);
            word;
            word = strtok(NULL, sep)
         )
     {
     if (*word==0) continue;
        
        item.key = word;
        if ((found_item = hsearch(item, FIND)) != NULL) {
            // found
            node = found_item->data;
            fprintf(out, "%s", word);
            while ( node ) {
                fprintf(out, " %i", node->position);
                node = node->next;
            }
            fprintf(out, "\n");
        } else {
            printf("cagamos con '%s'\n", word);
        }
    }
    fclose(out);
}