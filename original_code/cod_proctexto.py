# procesamos la fuente
indice = {}
fuente = open("src_shakesp.txt").read()
fuente = fuente.replace("\n", " ")
fuente = fuente.replace("\r", " ")
largo = len(fuente)
ini = 0
pos = 1

while True:
    if pos >= largo:
        break
    f = fuente[pos]
    if f == " ":
        palabra = fuente[ini:pos]
        indice.setdefault(palabra, []).append(str(ini))
        ini = pos + 1
    pos += 1

# revisamos que tenemos que encontrar
dest = open("res_2.txt", "w")
for lin in open("src_tokens.txt"):
    lin = lin.strip()
    pos = indice[lin]
    dest.write(lin + " " + " ".join(pos) + "\n")
dest.close()