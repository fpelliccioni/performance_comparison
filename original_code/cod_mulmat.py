import sys
if len(sys.argv) != 3:
    print "Usar cod_mulmat.py LADO POT"
    exit()

LADO = int(sys.argv[1])
POT = int(sys.argv[2])

class Matriz(object):
    def __init__(self, lista):
        lista = list(lista)
#        assert len(lista) == LADO*LADO
        self._matriz = []
        for i in xrange(LADO):
            lin = lista[i*LADO:(i+1)*LADO]
            self._matriz.append(lin)
#        print self._matriz

    def __getitem__(self, ind):
        return self._matriz[ind]

    def __str__(self):
        imp = ""
        for i in xrange(LADO):
            imp += "".join(("%5d"%self[i][x]) for x in xrange(LADO)) + "\n"
        return imp

    def __repr__(self):
        return str(self._matriz)

    def __pow__(self, expon):
        m = self
        for i in xrange(expon-1):
            m = self.__mul__(m)
        return m

    def __mul__(self, other):
        if not isinstance(other, Matriz):
            raise TypeError("Las matrices se multiplican por matrices, boludo!")

        res = []
        for i in xrange(LADO): # self
            for j in xrange(LADO):
                # la linea de self por la columna de other
                r = 0
                self_i = self[i]
                for ind in xrange(LADO):
                    x = self_i[ind]
                    y = other[ind][j]
#                    print "mul", x, y
                    r += x * y
                res.append(r)
        m = Matriz(res)
        return m

arch = open("src_mulmat.txt")
m = Matriz(int(x) for x in arch)
m**POT