
#include <stdio.h>
#include <openssl/bn.h>
#include <openssl/crypto.h>
#include <strings.h>
#include <Python.h>

typedef struct _matrix {
    int x_size;
    int y_size;
    BIGNUM **data;    
} matrix;

matrix *matrix_new(int x_size, int y_size) {
    matrix *mat;
    
    if ( (mat = malloc( sizeof(matrix) )) == 0 ) {   
        fprintf(stderr, "Alloc error 1\n");
        return NULL;
    }
    if ( (mat->data = malloc( sizeof(BIGNUM *)*x_size*y_size )) == 0 ) {   
        fprintf(stderr, "Alloc error 2\n");
        free(mat);
        return NULL;
    }
    mat->x_size = x_size;
    mat->y_size = y_size;
    return mat;
}


void matrix_set(matrix *mat, int x, int y, BIGNUM *value) {
    if (mat->data[x+y*(mat->x_size)] == NULL) {
        mat->data[x+y*(mat->x_size)] = BN_new();
    }
    mat->data[x+y*(mat->x_size)] = BN_dup(value);
//    BN_copy(mat->data[x+y*(mat->x_size)], value);
}

BIGNUM *matrix_get(matrix *mat, int x, int y) {
    return mat->data[x+y*(mat->x_size)];
}
matrix *matrix_copy(matrix *mat) {
    matrix *newmat;
    int x,y;
    newmat = matrix_new(mat->x_size, mat->y_size);
    for (y = 0; y < mat->y_size; y++) {
        for (x = 0; x < mat->x_size; x++) {
            matrix_set( newmat, x, y, matrix_get( mat, x, y) );
        }
    }
    return newmat;
}

void matrix_mul(matrix *one, matrix *two, matrix *out) {
    int x, y, i;
    
    BIGNUM *temp_add;
    BIGNUM *temp_mul;

    BN_CTX *bnctx;
    
    bnctx = BN_CTX_new();
    BN_CTX_init( bnctx );
    
    temp_mul = BN_new();
    temp_add = BN_new();
    for (y = 0; y < out->y_size; y++) {
        for (x = 0; x < out->x_size; x++) {
            //por cada celda del resulta
            //temp_add = BN_new();
            BN_zero(temp_add);
            for (i = 0; i < out->x_size; i++ ) {
                
                BN_mul( 
                    temp_mul,
                    matrix_get(one, i, y),
                    matrix_get(two, x, i), 
                    bnctx
                );
                BN_add(temp_add, temp_add, temp_mul);
            }
            matrix_set(out, x, y, temp_add);
        }
    }
}

void matrix_print(matrix *mat) {
    int x, y;

    for (y = 0; y < mat->y_size; y++) {
        for (x = 0; x < mat->x_size; x++) {
            printf("%s ", BN_bn2dec( matrix_get(mat, x, y) ) );
        }
        printf("\n");
    }
}

matrix *matrix_load(char *filename, int x_size, int y_size) {
    FILE *in;
    int i, x, y;
    BIGNUM *num;
    char buf[1024]; // hack :)
    matrix *mat;
    
    in  = fopen(filename, "r");
    mat = matrix_new(x_size, y_size);
    
    for (y = 0; y < mat->y_size; y++) {
        for (x = 0; x < mat->x_size; x++) {
            bzero(buf, 1024);
            i = 0;
            while ( 1 ) {
                fread(buf+i, 1,1, in);
                if (buf[i]=='\n') break;
                i ++;
            }
            num = BN_new();
            buf[i]=0;
            BN_dec2bn(&num, buf);
            matrix_set(mat, x, y, num);
        }
    }    
    return mat;
}


void old_main(char *nomarch, int potencia, int LADO) {
    matrix *mat;
    matrix *two;
    matrix *res;
    int i;
       
    mat = matrix_load(nomarch, LADO, LADO);
    res = matrix_new(LADO, LADO);    
    two = matrix_copy(mat);    
    
    for (i=1;i<potencia ;) {
        matrix_mul(mat, two, res);
        printf("res (pot %d): %s\n", i, BN_bn2dec( matrix_get(res, 0,0)) );
        i++;
        if (i >= potencia) break;
        matrix_mul(mat, res, two);
        printf("res (pot %d): %s\n", i, BN_bn2dec( matrix_get(two, 0,0)) );
        i++;
    }
}

// Lo siguiente es maquinaria para que podamos llamar
// a estas funciones desde Python...

static PyObject *prueba_mulmat(PyObject *, PyObject *);

static PyMethodDef MetodosMulmat[] = {
    {"mulmat",  prueba_mulmat, METH_VARARGS, "Levanta una matriz de N lados del archivo y la eleva a M"},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};


static PyObject *
prueba_mulmat(PyObject *self, PyObject *args)
{
    char *nomarch;
    int lado;
    int potencia;

    if (!PyArg_ParseTuple(args, "sii", &nomarch, &lado, &potencia))
        return NULL;

    old_main(nomarch, potencia, lado);
    return Py_BuildValue("i", 0);
}

PyMODINIT_FUNC
initcod_mulmat_py(void)
{
    (void) Py_InitModule("cod_mulmat_py", MetodosMulmat);
}

/*

Para compilarlo:

gcc -I /usr/include/python2.5/ -c cod_mulmat_py.c -o cod_mulmat_py.o
ld -shared -lpython2.5 cod_mulmat_py.o -lcrypto -o cod_mulmat_py.so
*/